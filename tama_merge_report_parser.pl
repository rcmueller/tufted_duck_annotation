#!/usr/bin/perl

use strict;
use warnings;
use Data::Dumper qw(Dumper);

###
# parse TAMA merge gene/transcript report.txt file
# write comma-separated matrix, e.g., for UpSetR with Gene/Transcript and binary true/false
# for gene/transcript support in all pipeline-tissue combinations
# 20200901, rmueller
# 20200902, rmueller: added extra columns to plot as attributes in UpSetR (currently just for gene report)
###

my ($inFile, $outFile) = @ARGV;

die "\n\tUsage: $0 <gene/transcript_report.txt> <outfile.csv>\n\n" if (not defined $inFile);
die "\n\tUsage: $0 <gene/transcript_report.txt> <outfile.csv>\n\n" if (not defined $outFile);

open(my $fh, $inFile) or die "\n\tCould not open '$inFile' $!\n\n";
open(my $wfh, '>', $outFile) or die "\n\tCould not open '$outFile' $!\n\n";

my @pipelineTissue = ("illumina_brain", "illumina_ileum", "illumina_lung", "illumina_ovary", "illumina_spleen", "illumina_testes", "pacbio_brain", "pacbio_ileum", "pacbio_lung", "pacbio_ovary", "pacbio_spleen", "pacbio_testes");
my $reportFile; #gene or transcript report; checked at runtime (first word in line 1 of report file)
my $commaSepField; #in gene report, 4th column contains read support; in transcript report, it's the 3rd
my %parsedRecords = (); #report.txt ist first parsed into hash of arrays
my $recordKey; #key for the hashes
my @binaryTrueFalse = (); #1 -> read supported by pipeline-tissue combination; 0 -> not supported
my @tempArray = (); #stores read support (binary) for each pipeline-tissue combination
my %resultRecords = (); #results are stored in another, sorted hash (by key)


while (my $row = <$fh>) {
    # determine input file format (gene or transcript report)
    if ($. == "1") {
        if ($row =~ m/^gene_id/) {
            print "\n\t\"$inFile\" looks like a TAMA merge gene report file\n\n";
            $reportFile = "Gene";
            $commaSepField = 3; #0-based, 4th field in gene report
            next;
        } elsif ($row =~ m/^transcript_id/) {
            print "\n\t\"$inFile\" looks like a TAMA merge transcript report file\n\n";
            $reportFile = "Transcript";
            $commaSepField = 2; #0-based, 3th field in gene report
            next;
        } else {
            die "\n\t\"$inFile\" does not look like a TAMA merge report file\n\n";
            }
        }
    chomp $row;
    my @tabSep = split(/\t/,$row); #tab-separated fields
    my @commaSep = split(/,/,$tabSep[$commaSepField]); #also comma-separated field 3 in gene report (field 4 in transcript report)
    my $transcriptLength = $tabSep[6] - $tabSep[5]; #determine length of transcript
    push @commaSep, ($tabSep[1], $tabSep[2], $transcriptLength); #push num_clusters, num_final_transcripts, length_transcript (extra attributes)
    $parsedRecords{$tabSep[0]} = \@commaSep; #hash of arrays; key is gene/transcript id (field 1), value is array of pipeline_tissue
}

#print Dumper \%parsedRecords;

foreach $recordKey (keys %parsedRecords) { #every key in hash
    for my $seqTissue (@pipelineTissue) { #all pipeline-tissue combinations
        if (grep /$seqTissue/, @{ $parsedRecords{ $recordKey } }) {
            push @tempArray, 1; #binary true, if found
        } elsif (grep !/$seqTissue/, @{ $parsedRecords{ $recordKey } }) {
            push @tempArray, 0; #binary false, if not
        }
    }
    push @tempArray, ($parsedRecords{$recordKey}[-3], $parsedRecords{$recordKey}[-2], $parsedRecords{$recordKey}[-1]); #add attributes from parsedRecords to @tempArray (push)
    $resultRecords{$recordKey} = [ @tempArray ]; #write results in new hash

#    print $recordKey.": ";
#    foreach (@tempArray) {
#        print $_." ";
#    }
#    print "\n";

    @tempArray = (); #reset temp array
}

#print Dumper \%resultRecords;

print $wfh $reportFile.",".join(',',@pipelineTissue).",num_clusters,num_final_transcripts,transcript_length\n";

foreach $recordKey (sort { substr($a, 1) <=> substr($b, 1) } keys (%resultRecords)) { #for correct sorting, leading "G" has to be ignored
    @tempArray = @{ $resultRecords{$recordKey} }; #dereference array of hash?
    print $wfh $recordKey;
    foreach (@tempArray) {
        print $wfh ",".$_;
    }
    print $wfh "\n";
}

close $wfh;
