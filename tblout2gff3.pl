#!/usr/bin/perl

use strict;
use warnings;

###
# convert Infernal's cmscan and cmsearch *.tblout into gff3 (autodetect tblout-format)
# flip start and end if feature is on "-" strand (infernal writes start > end in this case)
# reject hits with E-value > 5.0e-4 (0.0005)
# 20200612, rmueller
# 20201014, rmueller, remove lower-scoring overlaps ("=" in column 20)
# 20201014, rmueller, remove double quotation marks from Name tag (column 9, 'Name=%s' instead of 'Name=\"%s\"')
##

my ($inFile, $outFile) = @ARGV;

die "\n\tUsage: $0 <tblout-file> <gff-file>\n\n" if (not defined $inFile);
die "\n\tUsage: $0 <tblout-file> <gff-file>\n\n" if (not defined $outFile);

open(my $fh, $inFile) or die "\n\tCould not open '$inFile' $!\n\n";
open(my $wfh, '>', $outFile) or die "\n\tCould not open '$outFile' $!\n\n";

my $cmTblFormat = "";
my $rejectThreshold = 5.0e-4;
my $rejectCount = 1;

while (my $row = <$fh>) {
	chomp $row;
	# determine output table format (from cmscan or cmsearch)
	if ($. == "1") {
		if ($row =~ m/^#idx/) {
			print "\n\tTreat $inFile like a cmscan tblout file\n";
			$cmTblFormat = "cmscan";
			next;
		} elsif ($row =~ m/^#target/) {
			print "\n\tTreat $inFile like a cmsearch tblout file\n";
			$cmTblFormat = "cmsearch";
			next;
		} else {
			die "\n\t$inFile does not look like cmscan or cmsearch tblout file\n\n";
		}
	}
	next if ($row =~ m/^#| = /);
	# split cmscan-output into 27 space-separated fields
	if ($cmTblFormat eq "cmscan") {
		my @fields = split / +/, $row, 27;
		# reject hits with E-value > 5.0e-5
		if ($fields[17] > $rejectThreshold) {
			$rejectCount += 1;
			next;
		}
		# flip start and end if feature is on negative strand
		if ($fields[11] eq "-") {
			printf $wfh "%s\t$cmTblFormat\t%s\t%s\t%s\t%s\t%s\t.\tID=%s;Parent=%s;Name=%s;Note=%s\n", $fields[3], $fields[1], $fields[10], $fields[9], $fields[16], $fields[11], $fields[2], $fields[5], $fields[26], $fields[17];
		} elsif ($fields[11] eq "+") {
			printf $wfh "%s\t$cmTblFormat\t%s\t%s\t%s\t%s\t%s\t.\tID=%s;Parent=%s;Name=%s;Note=%s\n", $fields[3], $fields[1], $fields[9], $fields[10], $fields[16], $fields[11], $fields[2], $fields[5], $fields[26], $fields[17];
		}
	} 
	# ... similar for cmsearch-output (18 fields)
	if ($cmTblFormat eq "cmsearch") {
		my @fields = split / +/, $row, 18;
		if ($fields[15] > $rejectThreshold) {
			$rejectCount += 1;
			next;
		}
		if ($fields[9] eq "-") {
			printf $wfh "%s\t$cmTblFormat\t%s\t%s\t%s\t%s\t%s\t.\tID=%s;Name=%s;Note=%s\n", $fields[0], $fields[2], $fields[8], $fields[7], $fields[14], $fields[9], $fields[3], $fields[17], $fields[15];
		}
		if ($fields[9] eq "+") {
			printf $wfh "%s\t$cmTblFormat\t%s\t%s\t%s\t%s\t%s\t.\tID=%s;Name=%s;Note=%s\n", $fields[0], $fields[2], $fields[7], $fields[8], $fields[14], $fields[9], $fields[3], $fields[17], $fields[15];
		}
	}
}
print "\tRejected $rejectCount records with E-value > $rejectThreshold\n";
close $wfh;
print "\tDone writing to $outFile\n\n";
