## R

*Analyses/graphs in R and input files for protein-coding potential, ncRNA, read support and structural annotation statistics.*

## Tama merge report parser

*Prepare [TAMA](https://doi.org/10.1186/s12864-020-07123-7) merge gene report for [UpSetR](https://doi.org/10.1093/bioinformatics/btx364).*

TAMA merge allows you to merge multiple transcriptomes. In addition to merging annotations, `tama_merge.py` also creates a gene report containing each
feature's source (in our study: pipeline and tissue). This gene report can be parsed with `tama_merge_report_parser.pl`
as preparation for UpSetR: each feature will get a binary TRUE or FALSE label assigned depending on the support of each source.

## Tblout to gff3 converter

*Convert `cmscan` output from the [Infernal](https://doi.org/10.1093/bioinformatics/btt509) software suite to generic feature format version 3.*

Small RNA from the [Rfam](https://rfam.xfam.org) database can be scanned in genomes and transcriptomes using Infernal's `cmscan`.
This tool produces a space-separated table which can be converted using `tblout2gff3.pl`. This script also removes lower-scoring
overlaps and hits with E-value larger than 5.0E-4.

## Citation

Please cite **Mueller, R.C. et al. (2021) ‘A high-quality genome and comparison of short- versus long-read transcriptome of the palaearctic duck Aythya fuligula (tufted duck)’, GigaScience, 10(12), p. giab081.** Available at: [https://doi.org/10.1093/gigascience/giab081](https://doi.org/10.1093/gigascience/giab081).
